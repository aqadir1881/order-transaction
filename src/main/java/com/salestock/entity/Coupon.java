package com.salestock.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by abdulqadir on 8/29/16.
 */

@Entity
@Table(name = "coupon")
public class Coupon {

    @Id
    @Column(name = "id")
    private long id;

    @Column(name = "type")
    private String type;

    @Column(name = "amount")
    private Double amount;

    @Column(name = "ispercentage")
    private boolean isPercentage;
}
